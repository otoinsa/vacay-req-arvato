# What's this?

![](scrnshot.png)

Use this to generate Arvato-ish .docx forms for vacations in batch / single-mode, or to generate a birthday leave request docx.
Made for people without MS Word or just lazies :P

## Start

You'll need [nodejs](https://nodejs.org/en/download/) on your machine.

```
npm i
npm run vacation
```

To generate a birthday leave .docx:

```
npm run birthdayLeave
```

You'll find the output at `./out`,
edit the *.docx files under `./templates` as you please.

## Batching this...
Edit `./vacations.json`:

```
// vacations.json
{
    "fullName": "John Doe",
    "vacations": [
        {
            "start_date": "11/11/2020", // DD/MM/YYY
            "end_date": "15/11/2020"
        },
        {
            "start_date": "11/11/2020",
            "end_date": "15/11/2020"
        },
        {
            "start_date": "11/11/2020",
            "end_date": "15/11/2020"
        }
    ]
}
```
and run
```
npm run vacation
```