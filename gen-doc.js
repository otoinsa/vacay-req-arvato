var PizZip = require('pizzip');
var Docxtemplater = require('docxtemplater');
const prompts = require('prompts');
const moment = require('moment');
var fs = require('fs');
var path = require('path');
const vacationsJson = require('./vacations.json');
const commandLineArgs = require('command-line-args');

const args = commandLineArgs([{name: 'template', type: String, multiple: false}])

// The error object contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
function replaceErrors(key, value) {
    if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function(error, key) {
            error[key] = value[key];
            return error;
        }, {});
    }
    return value;
}

function errorHandler(error) {
    console.log(JSON.stringify({error: error}, replaceErrors));

    if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors.map(function (error) {
            return error.properties.explanation;
        }).join("\n");
        console.log('errorMessages', errorMessages);
        // errorMessages is a humanly readable message looking like this :
        // 'The tag beginning with "foobar" is unopened'
    }
    throw error;
}

//Load the docx file as a binary
const template = args.template || 'Vacation';
var content = fs
    .readFileSync(path.resolve(__dirname, `templates/${template}.docx`), 'binary');

function makeFile(data){
    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }
    
    //set the templateVariables
    doc.setData(data);
    
    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }
    
    var buf = doc.getZip()
                 .generate({type: 'nodebuffer'});
    
    // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(__dirname, `out/${data.fileName}.docx`), buf);
    
    //can copy this line for reference
    console.log(`${data.start_date} to ${data.end_date} (${data.work_days} working days)`);    
}

function workday_count(start,end) {
    var first = start.clone().endOf('week'); // end of first week
    var last = end.clone().startOf('week'); // start of last week
    var days = last.diff(first,'days') * 5 / 7; // this will always multiply of 7
    var wfirst = first.day() - start.day(); // check first week
    if(start.day() == 0) --wfirst; // -1 if start with sunday 
    var wlast = end.day() - last.day(); // check last week
    if(end.day() == 6) --wlast; // -1 if end with saturday
    return wfirst + Math.floor(days) + wlast; // get the total
  } //  

(async () => {
    let data = {} //template variables from .docx like {last_name} etch

    const start = await prompts({
    type: 'text',
    name: 'value',
    message: 'Enter the vacation start date (DD/MM/YYYY) / or just hit enter to use the vacations.json'
    });

    if(typeof start.value === 'string' && start.value.length < 1){  //just input stuff - otherwise
        vacationsJson.vacations.forEach((vacation)=>{
            const startMoment = moment(vacation.start_date, 'DD/MM/YYYY')
            const endMoment = moment(vacation.end_date, 'DD/MM/YYYY')
            let data = {
                first_name: vacationsJson.fullName.split(' ')[0],
                last_name: vacationsJson.fullName.split(' ')[1],
                start_date: startMoment.format('DD/MM/YYYY'),
                end_date: endMoment.format('DD/MM/YYYY'),
                work_days: workday_count(startMoment, endMoment),
            }
            data.fileName = `${data.first_name}-${startMoment.format('DD-MM-YYYY')}-${endMoment.format('DD-MM-YYYY')}`

            makeFile(data)
        })
        return
    }else if(typeof start.value === 'undefined'){ // ctrl c happened probs
        return false
    }else{ // if need to gen 1 file use case continues...
        const startMoment = moment(start.value.toString(), 'DD/MM/YYYY')
        data.start_date = startMoment.format('DD/MM/YYYY')

        const end = await prompts({
        type: 'text',
        name: 'value',
        message: 'Enter the vacation end date (DD/MM/YYYY)'
        });

        const endMoment = moment(end.value.toString(), 'DD/MM/YYYY')
        data.end_date = endMoment.format('DD/MM/YYYY')

        // calculate work days
        data.work_days = workday_count(startMoment, endMoment);
        
        const fullName = await prompts({
        type: 'text',
        name: 'value',
        message: 'Enter your full name (Name Surname)'
        });

        data.first_name = fullName.value.split(' ')[0]
        data.last_name = fullName.value.split(' ')[1]

        data.fileName = `${data.first_name}-${startMoment.format('DD-MM-YYYY')}-${endMoment.format('DD-MM-YYYY')}`

        makeFile(data)
    }
})();